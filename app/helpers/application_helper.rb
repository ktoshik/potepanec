module ApplicationHelper
  def full_title(page_title: '')
    if page_title.blank?
      Constants::POTEPAN_EC_TITLE
    else
      "#{page_title} - #{Constants::POTEPAN_EC_TITLE}"
    end
  end
end
