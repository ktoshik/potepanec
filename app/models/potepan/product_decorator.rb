module Potepan::ProductDecorator
  def related_products
    return [] if taxon_ids.empty?

    Spree::Product.
      joins(:taxons).
      includes(master: [:images, :default_price]).
      where.not(id: id).
      in_taxons(taxon_ids).
      distinct.
      sample(Constants::RELATION_PRODUCTS_NUMBER)
  end

  Spree::Product.prepend self
end
