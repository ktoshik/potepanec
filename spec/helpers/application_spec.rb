require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "full_title" do
    subject { full_title(page_title: page_title) }

    let(:base_title) { "BIGBAG Store" }

    context "page_titleが存在する" do
      let(:product) { create(:product) }
      let(:page_title) { "Product" }

      it { is_expected.to eq "Product - #{base_title}" }
    end

    context "page_titleが存在しない" do
      let(:page_title) { nil }

      it { is_expected.to eq base_title }
    end

    context "page_titleが空白" do
      let(:page_title) { " " }

      it { is_expected.to eq base_title }
    end
  end
end
