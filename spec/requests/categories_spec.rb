require 'rails_helper'

RSpec.describe "CategoriesRequests", type: :request do
  describe "商品カテゴリーページへアクセス(potepan/categories#show)" do
    let!(:product) { create(:product, taxons: [taxon]) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:taxonomy) { create(:taxonomy) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "HTTP status code 200 を返す" do
      expect(response).to have_http_status(200)
    end

    it "Shop,taxon名,taxonomy名,商品名と商品価格を返す" do
      expect(response.body).to match(/<a href="#">Shop<\/a>/)
      expect(response.body).to include taxon.name
      expect(response.body).to include taxonomy.name
      expect(response.body).to include product.name
      expect(response.body).to include product.display_price.to_s
    end

    it "taxonomiesを@taxonomiesに割り当てる" do
      expect(controller.instance_variable_get(:@taxonomies)).to match_array taxonomy
    end

    it "taxonを@taxonに割り当てる" do
      expect(controller.instance_variable_get(:@taxon)).to eq taxon
    end

    it "productsを@productsに割り当てる" do
      expect(controller.instance_variable_get(:@products)).to match_array product
    end
  end
end
