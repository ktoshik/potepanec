require 'rails_helper'

RSpec.describe "ProductsRequests", type: :request do
  include ApplicationHelper
  describe "商品詳細ページへアクセス (potepan/products#show)" do
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:taxon) { create(:taxon) }
    let(:related_products) { create_list(:product, 2, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it "HTTP status code 200 を返す" do
      expect(response).to have_http_status(200)
    end

    it "商品名,商品価格,商品説明を返す" do
      expect(response.body).to include product.name
      expect(response.body).to include product.display_price.to_s
      expect(response.body).to include product.description
    end

    it "productを@productに割り当てる" do
      expect(controller.instance_variable_get(:@product)).to eq product
    end

    it "related_productsを@related_productsに割り当てる" do
      controller.instance_variable_set(:@related_products, related_products)
      expect(controller.instance_variable_get(:@related_products)).to match_array related_products
    end
  end
end
