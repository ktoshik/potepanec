require 'rails_helper'

RSpec.describe 'CategoriesSystem', type: :system do
  describe "カテゴリーページ" do
    let!(:product) { create(:product, taxons: [taxon]) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:taxonomy) { create(:taxonomy) }
    let(:product1) { create(:product, taxons: [taxon1], price: 11.11) }
    let(:taxon1) { create(:taxon) }

    before do
      visit potepan_category_path(taxon.id)
    end

    it "light_sectionに商品名,Homeが表示されている" do
      within ".pageHeader" do
        expect(page).to have_selector "h2.page_heading", text: taxon.name
        expect(page).to have_link "Home"
        expect(page).to have_link "Shop"
        expect(page).to have_selector "li.active", text: taxon.name
      end
    end

    it "商品カテゴリーリストにtaxonomy,taxon名が表示されている" do
      within "#product_categories" do
        expect(page).to have_link taxonomy.name
        expect(page).to have_link taxon.name
      end
    end

    it "商品カテゴリーリストのリンク先がカテゴリーの商品一覧になっている" do
      within "#product_categories" do
        click_on taxon.name
        expect(current_path).to eq potepan_category_path(taxon.id)
      end
    end

    it "色から探すリストに各色が表示されている" do
      within "#find_by_color" do
        expect(page).to have_link "ブラック"
        expect(page).to have_link "ホワイト"
        expect(page).to have_link "レッド"
        expect(page).to have_link "ブルー"
        expect(page).to have_link "オレンジ"
      end
    end

    it "サイズから探すリストに各サイズが表示されている" do
      within "#find_by_size" do
        expect(page).to have_link "S"
        expect(page).to have_link "M"
        expect(page).to have_link "L"
        expect(page).to have_link "XL"
      end
    end

    it "並び替えのドロップリストに各種並び替えが表示されている" do
      within "#drop_list" do
        expect(page).to have_select("guiest_id1", options: ["並び替え", "人気順", "安い順", "新着順", "高い順"])
      end
    end

    it "商品一覧に商品名と商品価格が表示されている" do
      within "#product_images" do
        expect(page).to have_link product.name
        expect(page).to have_link product.display_price.to_s
        expect(page).not_to have_link product1.name
        expect(page).not_to have_link product1.display_price.to_s
      end
    end

    it "商品一覧のリンク先が商品詳細ページになっている" do
      within "#product_images" do
        click_on product.name
        expect(current_path).to eq potepan_product_path(product.id)
        visit potepan_category_path(taxon.id)
        click_on product.display_price.to_s
        expect(current_path).to eq potepan_product_path(product.id)
      end
    end
  end
end
