require 'rails_helper'

RSpec.describe 'ProductsSystem', type: :system do
  describe "商品詳細ページ" do
    let(:product) { create(:product, taxons: [taxon]) }
    let(:taxon) { create(:taxon) }
    let!(:related_products) { create_list(:product, 2, taxons: [taxon]) }

    before do
      visit potepan_product_path(product.id)
    end

    it "light_sectionに商品名,Homeが表示されている" do
      within ".pageHeader" do
        expect(page).to have_selector "h2.page_heading", text: product.name
        expect(page).to have_link "Home"
        expect(page).to have_selector "li.active", text: product.name
      end
    end

    it "商品名,商品価格,商品説明が表示されている" do
      within "#product_summary" do
        expect(page).to have_selector "h2", text: product.name
        expect(page).to have_selector "h3", text: product.display_price
        expect(page).to have_selector "p", text: product.description
      end
    end

    it "一覧ページへ戻るリンクが表示されている" do
      within "#link_list_page" do
        expect(page).to have_link "一覧ページへ戻る"
      end
    end

    it "一覧ページへ戻るのリンク先がカテゴリーの商品一覧になっている" do
      within "#link_list_page" do
        click_on "一覧ページへ戻る"
        expect(current_path).to eq potepan_category_path(product.taxons.first.id)
      end
    end

    it "サイズのドロップリストに各サイズが表示されている" do
      within "#size_drop_list" do
        expect(page).to have_select("guiest_id3", options: ["S", "M", "L", "XL"])
      end
    end

    it "数量のドロップリストに数量が表示されている" do
      within "#quantity_drop_list" do
        expect(page).to have_select("guiest_id4", options: ["1", "2", "3"])
      end
    end

    it "関連商品の商品名,商品価格が表示されている" do
      within "#related_products_form" do
        related_products.each do |related_product|
          expect(page).to have_link related_product.name
          expect(page).to have_link related_product.display_price.to_s
        end
      end
    end

    it "関連商品のリンク先が商品詳細になっている" do
      within "#related_products_form" do
        related_products.each do |related_product|
          click_on related_product.name
          expect(current_path).to eq potepan_product_path(related_product.id)
        end
      end
    end
  end
end
