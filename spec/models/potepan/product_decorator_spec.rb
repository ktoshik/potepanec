require 'rails_helper'

RSpec.describe Potepan::ProductDecorator, type: :model do
  describe "related_products" do
    subject { product.related_products }

    let!(:product) { create(:product, taxons: [taxon]) }
    let(:taxon) { create(:taxon) }

    context "カテゴリーが共通する商品がないとき" do
      let(:not_related_product) { create(:product, taxons: [taxon1]) }
      let(:taxon1) { create(:taxon) }

      it { is_expected.to eq [] }
    end

    context "カテゴリーが共通する商品があるとき" do
      let!(:related_products_for_test) { create(:product, taxons: [taxon]) }

      describe "関連商品に商品が表示される" do
        it { is_expected.to match_array related_products_for_test }
      end

      describe "メソッドをコールしている商品は含まれない" do
        it { is_expected.not_to include product }
      end
    end

    context "カテゴリーが共通する商品が5件あるとき" do
      let!(:related_products_for_test) { create_list(:product, 5, taxons: [taxon]) }

      it "表示されている関連商品は4件である" do
        # Constants::RELATION_PRODUCTS_NUMBER = 4
        expect(product.related_products.count).to eq Constants::RELATION_PRODUCTS_NUMBER
      end
    end
  end
end
